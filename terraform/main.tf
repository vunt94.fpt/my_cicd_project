// Provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_instance" "instance" {
  ami                    = var.instance-ami
  instance_type          = var.instance-type
  key_name               = aws_key_pair.key-pair.key_name
  vpc_security_group_ids = ["${aws_security_group.instance-sg.id}"]
  subnet_id              = aws_subnet.subnet.id
  tags = {
    Name = var.instance-tag-name
  }
}

resource "aws_security_group" "instance-sg" {
  name   = var.sg-name
  vpc_id = aws_vpc.vpc.id

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "22"
    to_port     = "22"
  }

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "80"
    to_port     = "80"
  }

  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    to_port     = "0"
  }

  tags = {
    "Name" = var.sg-name
  }
}
/// Key pair
// VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc-cidr-block
  enable_dns_hostnames = var.enable-dns-hostnames
  tags = {
    Name = var.vpc-name
  }
}

resource "aws_subnet" "subnet" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.subnet-cidr-block

  tags = {
    Name = var.subnet-name
  }
}

resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = var.ig-name
  }
}

resource "aws_default_route_table" "rt" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id
  # vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = var.rt-cidr-block
    gateway_id = aws_internet_gateway.ig.id
  }
}

resource "aws_key_pair" "key-pair" {
  key_name   = "my-key-pair"
  public_key = file("~/.ssh/my-key-pair.pub")
}

resource "aws_eip" "my_eip" {
  vpc = true
}

resource "aws_eip_association" "my_eip_assoc" {
  instance_id   = aws_instance.instance.id
  allocation_id = aws_eip.my_eip.id
}