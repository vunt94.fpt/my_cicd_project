variable "instance-ami" {
  type    = string
  default = "ami-0ae716ba0225afa06"
}

variable "instance-type" {
  type    = string
  default = "t2.micro"
}

variable "instance-tag-name" {
  description = "instance-tag-name"
  type        = string
  default     = "VuNT35-CICD-Project"
}

variable "sg-name" {
  description = "The Name to apply to the security group"
  type        = string
  default     = "VuNT35-SG-Name"
}

variable "vpc-cidr-block" {
  description = "The CIDR block to associate to the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet-cidr-block" {
  description = "The CIDR block to associate to the subnet"
  type        = string
  default     = "10.0.0.0/20"
}


variable "rt-cidr-block" {
  description = ""
  type        = string
  default     = "0.0.0.0/0"
}

variable "enable-dns-hostnames" {
  description = ""
  type        = bool
  default     = true
}

variable "vpc-name" {
  description = "The Name to apply to the VPC"
  type        = string
  default     = "VPC-created-with-terraform"
}

variable "subnet-name" {
  description = "The Name to apply to the Subnet"
  type        = string
  default     = "MySubnet"
}

variable "ig-name" {
  description = "The name to apply to the Internet gateway tag"
  type        = string
  default     = "MyIG"
}

variable "region" {
  description = "The region where ec2 is created"
  type        = string
  default     = "us-east-1"
}

variable "access_key" {
  description = ""
  type        = string
}

variable "secret_key" {
  description = ""
  type        = string
  sensitive   = true
}